package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;

// TODO: complete me :)
public class SpellbookAdapter implements Weapon {

    private Spellbook spellbook;

    public SpellbookAdapter(Spellbook spellbook) {

    }

    @Override
    public String normalAttack() {
        return null;
    }

    @Override
    public String chargedAttack() {
        return null;
    }

    @Override
    public String getName() {
        return null;
    }

    @Override
    public String getHolderName() {
        return null;
    }

}
